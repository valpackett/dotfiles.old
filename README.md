# dotfiles

Modular dotfiles!

- A module is a directory with an `apply.sh` file that installs the dotfiles the module contains
- `install.sh` installs multiple modules on the local machine
- `rinstall.sh` installs multiple modules on a remote machine using SSH

## Preferences

- XDG-style `~/.config/application-name/config-file-name` paths are preferred
- Binaries are placed into `~/.local/bin` (and `go`, `pip`, `npm`, `cpan`, `cargo`, etc. are configured to use that directory)
- Repos are placed into `~/src` using Go conventions (e.g. `~/src/github.com/unrelentingtech/dotfiles`). `$GOPATH` is `~` and [ghq] is used to clone non-Go repos there
- Keyboard configuration is mostly based on [A Modern Space Cadet]

[ghq]: https://github.com/motemen/ghq
[A Modern Space Cadet]: http://stevelosh.com/blog/2012/10/a-modern-space-cadet/

## Module list

### Common

- **desktop** -- common freedesktop things: fontconfig, XCompose..
- **dev-base** -- configs for [ssh], [git], [ctags], [curl] and other small but essential programs
- **bin** -- various useful scripts that mostly work
- **tmux** -- [tmux] configuration and command helper
- **zsh** -- [Z Shell] configuration and plugins

[ssh]: https://www.openssh.com
[git]: https://git-scm.com
[ctags]: http://ctags.sourceforge.net
[curl]: https://curl.se
[tmux]: https://github.com/tmux/tmux/wiki
[Z Shell]: https://www.zsh.org

### Editors

- **helix** -- [helix] text editor configuration
- **kakoune** -- [kakoune] text editor configuration
- **vim** -- [neovim] text editor configuration and plugins
- **emacs** -- [GNU Emacs] text editor configuration and plugins

[helix]: https://helix-editor.com
[kakoune]: https://kakoune.org
[neovim]: https://github.com/neovim/neovim
[GNU Emacs]: https://www.gnu.org/software/emacs/

### Languages

- **python** -- [Python] REPL configuration
- **ruby** -- [Ruby] irb, Rails, RubyGems configuration
- **node** -- [Node.js] npm configuration
- **ocaml** -- [OCaml]'s [OPAM] and [utop] configuration

[Python]: https://www.python.org
[Ruby]: https://www.ruby-lang.org/en/
[Node.js]: https://nodejs.org/en/
[OCaml]: https://ocaml.org
[OPAM]: https://opam.ocaml.org
[utop]: https://github.com/diml/utop
