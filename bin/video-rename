#!/usr/bin/env ruby
require 'date'
require 'json'

PATH = ARGV[0]

def files_at(path)
  Dir.entries(path)
    .select { |f| File.file? File.join(path, f) }
end

def get_date(path)
  ff = JSON.parse(IO.popen(['ffprobe', '-v', 'quiet', path, '-print_format', 'json', '-show_entries', 'format_tags=creation_time,date']).read)
  tags = ff['format']['tags'] || {}
  date = tags['creation_time'] || tags['date']
  return nil if date.nil?
  DateTime.parse(date) 
end

def rename(path)
  if File.basename(path).scan(/(?:Vid|VID)_(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})(\d{2})/).length == 1
    puts "=> #{path} skipped: Vid/VID already"
    return
  end

  dt = get_date(path)
  if dt.nil?
    puts "=> #{path} skipped: no date"
    return
  end
  new_path = File.join(File.dirname(path), "Vid_#{dt.strftime '%Y%m%d_%H%M%S'}#{File.extname(path)}")

  if new_path == path
    puts "=> #{path} skipped: same name"
    return
  end

  i = 0
  while File.exists?(new_path)
    puts "=> #{path}: #{new_path} exists"
    new_path = File.join(File.dirname(path), "Vid_#{dt.strftime '%Y%m%d_%H%M%S'}_#{i}#{File.extname(path)}")
    i += 1
  end

  puts "=> #{path} -> #{new_path}"
  File.rename(path, new_path)
end

if File.directory?(PATH)
  files_at(PATH).each do |path|
    rename(File.join(PATH, path))
  rescue Errno::ENOENT
    puts "#{path}: enoent"
  end
else
  rename(PATH)
end
