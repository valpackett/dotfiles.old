local wezterm = require 'wezterm'

return {
	-- window_decorations = "NONE",
	-- tab_bar_at_bottom = true,
	font = wezterm.font("monospace"),
	use_fancy_tab_bar = true,
	window_padding = { top = 0, right = 0, bottom = 0, left = 0 },
	window_background_opacity = 0.98,
	scrollback_lines = 65536,
	exit_behavior = "Close",
	enable_wayland = true,
	enable_kitty_graphics = true,
	quick_select_alphabet = "arstqwfpzxcvneioluymdhgjbk",
	mouse_bindings = {
		{ event = {Up  ={streak=1, button="Left"}}, mods = "NONE", action = wezterm.action{CompleteSelection="PrimarySelection"} },
		{ event = {Up  ={streak=1, button="Left"}}, mods = "CTRL", action = "OpenLinkAtMouseCursor" },
		{ event = {Down={streak=1, button="Left"}}, mods = "CTRL", action = "Nop" },
		{ event = {Down={streak=4, button="Left"}}, mods = "NONE", action = {SelectTextAtMouseCursor="SemanticZone"} },
	},
	key_map_preference = "Physical",
	leader = { key = "q", mods = "CTRL", timeout_milliseconds = 800 },
	keys = {
		{ key = "c", mods = "LEADER", action = wezterm.action{SpawnTab="CurrentPaneDomain"} },
		{ key = "x", mods = "LEADER", action = wezterm.action{CloseCurrentPane={confirm=true}} },
		{ key = "v", mods = "LEADER", action = wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}} },
		{ key = "d", mods = "LEADER", action = wezterm.action{SplitVertical={domain="CurrentPaneDomain"}} }, -- colemak:s
		{ key = "r", mods = "LEADER", action = wezterm.action{ActivateTabRelative=-1} }, -- colemak:p
		{ key = "j", mods = "LEADER", action = wezterm.action{ActivateTabRelative=1} }, -- colemak:n
		{ key = "h", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Left"} },
		{ key = "n", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Down"} }, -- colemak:k
		{ key = "y", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Up"} }, -- colemak:j
		{ key = "u", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Right"} },  --colemak:l
		{ key = "<", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Left", 2}} },
		{ key = "-", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Down", 2}} },
		{ key = "=", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Up", 2}} },
		{ key = "+", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Up", 2}} },
		{ key = ">", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Right", 2}} },
		{ key = "/", mods = "LEADER", action = wezterm.action{Search={CaseInSensitiveString=""}} },
		{ key = "?", mods = "LEADER", action = wezterm.action{Search={CaseInSensitiveString=""}} },
		{ key = "UpArrow",   mods = "SHIFT", action = wezterm.action{ScrollToPrompt=-1} },
		{ key = "DownArrow", mods = "SHIFT", action = wezterm.action{ScrollToPrompt=1} },
	},
	-- unix_domains = {
	-- 	{ name = "unix", connect_automatically = true },
	-- },
}
