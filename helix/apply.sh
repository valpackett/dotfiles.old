#!/bin/sh
echo "==> Installing helix"

mkdir -p ~/.config/helix/themes

cat config.toml > ~/.config/helix/config.toml
cat languages.toml > ~/.config/helix/languages.toml
cat base16.toml > ~/.config/helix/themes/base16_term_mod.toml

echo "==> Installed helix"
